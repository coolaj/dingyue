**快速订阅入口 以及 添加内置 Vless 节点信息：**

例如您的pages项目域名为：sub.fuck.cloudns.biz；

添加 TOKEN 变量，快速订阅访问入口，默认值为: auto ，获取订阅器默认节点订阅地址即 /auto ，例如 https://sub.fuck.cloudns.biz/auto
添加 HOST 变量，例如 edgetunnel-2z2.pages.dev；
添加 UUID 变量，例如 30e9c5c8-ed28-4cd9-b008-dc67277f8b02；
添加 PATH 变量，例如 /?ed=2048；

**添加你的专属优选线路：**

添加变量 ADD/ADDNOTLS 本地静态的优选线路，若不带端口号 TLS默认端口为443 / noTLS默认端口为80，#号后为备注别名，例如：
icook.tw:2053#优选域名
cloudflare.cfgo.cc#优选官方线路
添加变量 ADDAPI/ADDNOTLSAPI 为 优选IP地址txt文件 的 URL。例如：
https://raw.githubusercontent.com/cmliu/WorkerVless2sub/main/addressesapi.txt
https://raw.githubusercontent.com/cmliu/WorkerVless2sub/main/addressesipv6api.txt

**变量说明**

变量名	示例	备注
TOKEN	auto	快速订阅内置节点的订阅路径地址 /auto (支持多元素, 元素之间使用,作间隔)
HOST	edgetunnel-2z2.pages.dev	快速订阅内置节点的伪装域名
UUID	b7a392e2-4ef0-4496-90bc-1c37bb234904	快速订阅内置节点的UUID
PATH	/?ed=2560	快速订阅内置节点的路径信息
ADD	icook.tw:2053#官方优选域名	对应addresses字段 (支持多元素, 元素之间使用,作间隔)
ADDAPI	https://raw.github.../addressesapi.txt	对应addressesapi字段 (支持多元素, 元素之间使用,作间隔)
ADDNOTLS	icook.hk:8080#官方优选域名	对应addressesnotls字段 (支持多元素, 元素之间使用,作间隔)
ADDNOTLSAPI	https://raw.github.../addressesapi.txt	对应addressesnotlsapi字段 (支持多元素, 元素之间使用,作间隔)
ADDCSV	https://raw.github.../addressescsv.csv	对应addressescsv字段 (支持多元素, 元素之间使用,作间隔)
DLS	8	addressescsv测速结果满足速度下限
NOTLS	false	改为true, 将不做域名判断 始终返回noTLS节点
TGTOKEN	6894123456:XXXXXXXXXX0qExVsBPUhHDAbXXXXXqWXgBA	发送TG通知的机器人token
TGID	6946912345	接收TG通知的账户数字ID
SUBAPI	api.v1.mk	clash、singbox等 订阅转换后端
SUBCONFIG	https://raw.github.../ACL4SSR_Online_Full_MultiMode.ini	clash、singbox等 订阅转换配置文件
SUBNAME	WorkerVless2sub	订阅生成器名称
PS	【请勿测速】	节点名备注消息

